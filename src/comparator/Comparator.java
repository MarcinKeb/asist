package comparator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@MultipartConfig
public class Comparator extends HttpServlet
{
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        boolean isValid = true;

        Part[] files = {
            request.getPart("fileOne"),
            request.getPart("fileTwo")
        };

        List<String> filesContent = new ArrayList<>();

        List<BufferedReader> filesReader = new ArrayList<>();

        for (Part file : files) {
            if (validateFile(getFileName(file))) {
                BufferedReader reader = getContentFile(file);
                filesReader.add(reader);
                filesContent.add(reader.readLine());
            } else {
                isValid = false;
                setInformation(request, "danger", "Zły format lub brak pliku");
            }
        }

        if (isValid) {
            comparator(request, filesContent, filesReader);
        }

        RequestDispatcher rd=request.getRequestDispatcher("index.jsp");
        rd.forward(request, response);
    }

    private void comparator(HttpServletRequest request, List<String> filesContent, List<BufferedReader> filesReader) throws IOException
    {
        List<String> fileOneContentAfterCompare = new ArrayList<>();
        List<String> fileTwoContentAfterCompare = new ArrayList<>();

        String lineFileOne = filesContent.get(0);
        String lineFileTwo = filesContent.get(1);

        BufferedReader fileReaderOne = filesReader.get(0);
        BufferedReader fileReaderTwo = filesReader.get(1);

        boolean areEqual = compareLines(fileOneContentAfterCompare, fileTwoContentAfterCompare, lineFileOne, lineFileTwo, fileReaderOne, fileReaderTwo);

        areEqual(request, fileOneContentAfterCompare, fileTwoContentAfterCompare, areEqual);

        fileReaderOne.close();
        fileReaderTwo.close();
    }

    private boolean compareLines(List<String> fileOneContentAfterCompare, List<String> fileTwoContentAfterCompare, String lineFileOne, String lineFileTwo, BufferedReader fileReaderOne, BufferedReader fileReaderTwo) throws IOException
    {
        boolean areEqual = true;
        int iterator = 1;

        while (lineFileOne != null || lineFileTwo != null) {
            if (!Objects.equals(lineFileOne, lineFileTwo)) {

                if (lineFileOne != null && lineFileTwo != null) {
                    String[] itemsFileOne = lineFileOne.split(";");
                    String[] itemsFileTwo = lineFileTwo.split(";");

                    for (int lineNumber = 0; lineNumber < itemsFileOne.length; lineNumber++) {
                        if (!itemsFileOne[lineNumber].equals(itemsFileTwo[lineNumber])) {
                            itemsFileOne[lineNumber] = addLabel(itemsFileOne[lineNumber], "success");
                            itemsFileTwo[lineNumber] = addLabel(itemsFileTwo[lineNumber], "danger");
                        }
                    }

                    lineFileOne = String.join(";", itemsFileOne);
                    lineFileTwo = String.join(";", itemsFileTwo);
                } else if (lineFileOne != null) {
                    lineFileOne = addLabel(lineFileOne, "success");
                    lineFileTwo = addLabel(" ", "danger");
                } else {
                    lineFileOne = addLabel(" ", "danger");
                    lineFileTwo = addLabel(lineFileTwo, "success");
                }
                areEqual = false;
            }

            fileOneContentAfterCompare.add(iterator + ". " + lineFileOne + "<br>");
            fileTwoContentAfterCompare.add(iterator + ". " + lineFileTwo + "<br>");

            lineFileOne = fileReaderOne.readLine();
            lineFileTwo = fileReaderTwo.readLine();
            iterator++;
        }
        return areEqual;
    }

    private void areEqual(HttpServletRequest request, List<String> fileOneContentAfterCompare, List<String> fileTwoContentAfterCompare, boolean areEqual)
    {
        if (areEqual) {
            setInformation(request, "success", "Pliki są identyczne");
        } else {
            request.setAttribute("fileOneContentAfterCompare", fileOneContentAfterCompare);
            request.setAttribute("fileTwoContentAfterCompare", fileTwoContentAfterCompare);
        }
    }

    private BufferedReader getContentFile(Part file) throws IOException
    {
        InputStream inputStreamFile = file.getInputStream();
        return new BufferedReader(new InputStreamReader(inputStreamFile));
    }

    private boolean validateFile(String file)
    {
        return file.toLowerCase().endsWith("txt");
    }

    private String addLabel(String item, String label)
    {
        return "<span class='label label-"+ label + "'>" + item + "</span>";
    }

    private void setInformation(HttpServletRequest request, String type, String information)
    {
        request.setAttribute("information-" + type," <div class='alert alert-success'>" + information +"</div>");
    }

    private String getFileName(Part part)
    {
        String contentDisposition = part.getHeader("content-disposition");

        String[] tokens = contentDisposition.split(";");
        for (String token : tokens) {
            if (token.trim().startsWith("filename")) {
                return token.substring(token.indexOf("=") + 2, token.length()-1);
            }
        }
        return "";
    }
}
