<%-- Created by IntelliJ IDEA. --%>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-fingo-green bg-fingo-green" style="background-color: rgba(134,174,37,.85); color: #fff">
    <div class="col-md-2"><span class="navbar-brand">Komparator</span></div>
    <div class="col-md-2"></div>
    <div class="col-md-2"></div>
    <div class="col-md-2"></div>
    <div class="col-md-2">
        <img src="logo-fingo.svg" height="120" width="120" style="display: block;
                  margin-left: auto;
                  margin-right: auto;">
    </div>
    <div class="col-md-2">
        <img src="logo-asist.png" height="45" width="146" style="margin-top: 35px;
                  display: block;
                  margin-left: auto;
                  margin-right: auto;">
    </div>
</nav>

<form action="Comparator" method="post" enctype="multipart/form-data">
    <div class="container">
        <div class="row" id="row-1">
            <div class="col-sm-2"></div>
            <div class="col-sm-4">
                <div class="jumbotron">
                    <div class="form-group">
                        <label for="fileOne">Dodaj pierwszy plik</label>
                        <input type="file" class="form-control-file" id="fileOne" name="fileOne" aria-describedby="fileHelp">
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="jumbotron">
                    <div class="form-group">
                        <label for="fileTwo">Dodaj drugi plik</label>
                        <input type="file" class="form-control-file" id="fileTwo" name="fileTwo" aria-describedby="fileHelp">
                    </div>
                </div>
            </div>
            <div class="col-sm-2"></div>
        </div>
        <div class="row" id="row-2">
            <div class="col-sm-3"></div>
            <div class="col-sm-6 text-center" style="margin: 0 auto;">
                    <%
                        if(request.getAttribute("information-success") != null) out.println(request.getAttribute("information-success"));

                        if(request.getAttribute("information-danger") != null) out.println(request.getAttribute("information-danger"));
                    %>
            </div>
            <div class="col-sm-3"></div>
        </div>
        <div class="row" id="row-3">
            <div class="col-sm-5"></div>
            <div class="col-sm-2" style="margin: 0 auto;">
                <input type="submit" class="btn btn-primary" value="Porównaj dwa pliki">
            </div>
            <div class="col-sm-5"></div>
        </div>
        <div class="row" id="row-4" style="padding-top: 20px; padding-bottom: 20px">
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Plik 1</div>
                    <div class="panel-body"><%
                        if(request.getAttribute("fileOneContentAfterCompare") != null) out.println(request.getAttribute("fileOneContentAfterCompare").toString().replace(",", ""));
                    %></div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Plik 2</div>
                    <div class="panel-body"><%
                        if(request.getAttribute("fileTwoContentAfterCompare") != null) out.println(request.getAttribute("fileTwoContentAfterCompare").toString().replace(",", ""));
                    %></div>
                </div>
            </div>
        </div>
    </div>
</form>
<footer class="footer" style="
   position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   text-align: center;
   line-height: 20px;
   background-color: #f5f5f5;
">
    <div class="row">
        <div class="footer text-center">
            <p class="text-dark">Created by Marcin Kebłesz © 2018</p>
        </div>
    </div>
</footer>
</body>
</html>